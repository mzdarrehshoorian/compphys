""" Problem 4:  Numerics and plotting """

# import needed packages
import numpy as np
import matplotlib.pyplot as plt
from num_calculus import first_derivative # first_derivative function from num_calculus.py
from num_calculus import test_function # test_function (sine function) from num_calculus.py
from num_calculus import first_derivative_of_test_function # function calculates exact first derivatives from num_calculus.py
from num_calculus import trapezoid_integration # trapezoid_integration function from num_calculus.py
from num_calculus import integral_of_test_function # function calculates exact integration from num_calculus.py

# figure properties
plt.rcParams['legend.handlelength'] = 2
plt.rcParams['legend.numpoints'] = 1
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 12

fig = plt.figure()
ax = fig.add_subplot(111)

x = np.linspace(0, np.pi/2, 100)
f = test_function(x) # imported from num_calculus.py (sin(x) function)

# first derivative absolute error calculation
exact_first_derivatives_of_f = first_derivative_of_test_function(x) # calculating exact first derivatives of x using imported function first_derivative_of_test_function(x)
estimated_first_derivatives_of_f = np.zeros(len(x)) # setting zero as initial values, to be filled with estimated values later
for i in range(0, len(x)-1):
    estimated_first_derivatives_of_f[i] = first_derivative(test_function, x[i], x[i+1]-x[i]) # calculating estimated first derivatives of x using imported function first_derivative
error_first_derivative = np.abs(exact_first_derivatives_of_f - estimated_first_derivatives_of_f) # absolute error of first derivative


# Trapezoid integration absolute value calculation
exact_integration_of_f = np.zeros(len(x)) # setting zero as initial values, to be filled with exact values later
estimated_trapezoid_integration_of_f = np.zeros(len(x)) # setting zero as initial values, to be filled with estimated values later
for i in range(0, len(x)-1):
    estimated_trapezoid_integration_of_f[i] = trapezoid_integration(test_function, x[i:i+3]) # calculating estimated value of integral using Trapezoid integration function 
    exact_integration_of_f[i] = integral_of_test_function(x[i], x[i+1]) # calculating exact value of integral
error_trapezoid_integration = np.abs(exact_integration_of_f - estimated_trapezoid_integration_of_f)

# plot both curves into the same figure
ax.plot(x, error_first_derivative, label='First Derivative') 
ax.plot(x[:-2], error_trapezoid_integration[:-2], label='Trapezoid Integration') 
# include legend (with best location, i.e., loc=0)
ax.legend(loc=0) 
# set axes labels and limits
ax.set_xlabel(r'$dx$')
ax.set_ylabel(r'$Error$')
ax.set_ylim(0, 0.0004)
fig.tight_layout(pad=1)
fig.savefig('testfile.pdf',dpi=200)
plt.show()


