""" Problem 1: Integration """

# import needed packages
import numpy as np
from scipy.integrate import simps
from scipy.integrate import quad
from scipy.integrate import dblquad

## Part a)

# first integral

# estimating integral using Scipy
def integrand_a1(r): # integrand is defined
    return (r**2)*np.exp(-2*r) 
r_1a = np.linspace(0, np.inf, 100)
simp_int_a1 = simps(integrand_a1, r_1a) # estimated value of integral

# second integral

# estimating integral using Scipy
def integrand_a2(x): # integrand is defined
    return np.sin(x)/x 
x_1b = np.linspace(0,1,100)
simp_int_a2 = simps(integrand_a2, x_1b) # estimated value of integral

# third integral

# estimating integral using simpson integration of scipy
x_a3 = np.linspace(0, 5, 100) # x is defined (variable used in the third integral)
f_a3 = np.exp(np.sin(x_a3**3)) # integrand is defined
simp_int_a3 = simps(f_a3, x_a3) # estimated value of integral

## part b)

def integrand_b(x,y):
    return x*np.exp(-1*np.sqrt(x**2+y**2))
simp_int_b = dblquad(integrand_b, -2, 2, lambda x: 0, lambda x: 2)



""" Problem 4: Gradient and steepest descent """

## Part a)

#  implementation for N-dimensional numerical gradient is done in line 125 of num_calculus.py
from num_calculus import num_gradient

# test functions
x1_4a = [200, 2, 25, 89]
x2_4a = 10
dx = 0.1
def test_function1_4a(x): # number of dimension >= 3
    return np.sin(x[0])+np.cos(x[-1])+np.sum(x)-x[0]-x[-1]
def test_function2_4a(x): # number of dimension = 1
    return np.sin(x)

gradient_test1 = num_gradient(test_function1_4a, x1_4a, dx) 
gradient_test2 = num_gradient(test_function2_4a, x2_4a, dx)


## Part b)
 
def grad_desc(a, x, function, dx, eps):
    gamma = a/(len(x)+1) # Eq. 31
    grad_desc_x = x - gamma*num_gradient(function, x, dx) # Eq. 32
    while any(np.abs(grad_desc_x - x)> eps): # repaet the process until the difference of new x and old x is larger than epsilon
        x = grad_desc_x
        grad_desc_x = x - gamma*num_gradient(function, x, dx)
    return grad_desc_x


# test function
def test_function_4b(x):
    f = 0
    for i in x:
        f = f + (i**2)
    return f

dx = 0.01
eps = 0.0001
x_4b = np.ones(10).tolist()
grad_desc_x = grad_desc(0.1, x_4b, test_function_4b, dx, eps)


