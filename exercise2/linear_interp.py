"""
Linear interpolation in 1d, 2d, and 3d

Intentionally unfinished :)

Related to FYS-4096 Computational Physics
exercise 2 assignments.

By Ilkka Kylanpaa on January 2019
"""

# import needed packages
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


"""
Add basis functions l1 and l2 here
"""
def l1(t): # l1 is defined
    return 1-t
def l2(t): # l2 is defined
    return t


class linear_interp:

    def __init__(self,*args,**kwargs): # besides self, accepts an arbitrary number of arguments and/or keyword arguments
        self.dims=kwargs['dims']
        if (self.dims==1): # 1D : f(x)
            self.x=kwargs['x']
            self.f=kwargs['f']
            self.hx=np.diff(self.x) # hx = delta_x = x[i+1] - x[i]
        elif (self.dims==2): # 2D : f(x,y)
            self.x=kwargs['x']
            self.y=kwargs['y']
            self.f=kwargs['f']
            self.hx=np.diff(self.x) 
            self.hy=np.diff(self.y) # hy = delta_y = y[i+1] - y[i]
        elif (self.dims==3): # 3D : f(x,y,z)
            self.x=kwargs['x']
            self.y=kwargs['y']
            self.z=kwargs['z']
            self.f=kwargs['f']
            self.hx=np.diff(self.x) 
            self.hy=np.diff(self.y) 
            self.hz=np.diff(self.z) # hz = delta_z = z[i+1] - z[i]
        else:
            print('Either dims is missing or specific dims is not available')
      
    def eval1d(self,x): # 1D linear interpolation
        if np.isscalar(x):
            x=np.array([x]) # converting scalar to array
        N=len(self.x)-1 # number of data points
        f=np.zeros((len(x),)) # creating an array of zeros, to be filled later by f(x)'s
        ii=0
        for val in x:
            i=np.floor(np.where(self.x<=val)[0][-1]).astype(int)
            if i==N:
                f[ii]=self.f[i]
            else:
                t=(val-self.x[i])/self.hx[i] # calculating t = (x-x[i])/(x[i+1]-x[i])
                f[ii]=self.f[i]*l1(t)+self.f[i+1]*l2(t) # calculating S(x) = f[i]*l1+f[i+1]*l2
            ii+=1
        return f

    def eval2d(self,x,y): # 2D linear interpolation
        # converting scalar to array
        if np.isscalar(x):
            x=np.array([x]) 
        if np.isscalar(y):
            y=np.array([y])
        # number of data points
        Nx=len(self.x)-1
        Ny=len(self.y)-1
        # creating an array of zeros, to be filled later by f(x,y)'s
        f=np.zeros((len(x),len(y)))
        # creating an array of zeros, to be filled later by f[i,j], f[i,j+1], f[i+1,j] and f[i+1,j+1]
        A=np.zeros((2,2)) 
        ii=0
        for valx in x:
            i=np.floor(np.where(self.x<=valx)[0][-1]).astype(int)
            if (i==Nx):
                i-=1
            jj=0
            for valy in y:
                j=np.floor(np.where(self.y<=valy)[0][-1]).astype(int)
                if (j==Ny):
                    j-=1
                tx = (valx-self.x[i])/self.hx[i] # calculating tx = (x-x[i])/(x[i+1]-x[i])
                ty = (valy-self.y[j])/self.hy[j] # calculating ty = (y-y[i])/(y[i+1]-y[i])
                # calculating S(x,y) = [l1(tx),l2(tx)]A[l1(ty),l2(ty)]'
                ptx = np.array([l1(tx),l2(tx)])
                pty = np.array([l1(ty),l2(ty)])
                # filling matrix A by f[i,j], f[i,j+1], f[i+1,j] and f[i+1,j+1]
                A[0,:]=np.array([self.f[i,j],self.f[i,j+1]])
                A[1,:]=np.array([self.f[i+1,j],self.f[i+1,j+1]])
                f[ii,jj]=np.dot(ptx,np.dot(A,pty))
                jj+=1
            ii+=1
        return f
    #end eval2d

    def eval3d(self,x,y,z): # 3D linear interpolation
        # converting scalar to array
        if np.isscalar(x):
            x=np.array([x])
        if np.isscalar(y):
            y=np.array([y])
        if np.isscalar(z):
            z=np.array([z])
        # number of data points
        Nx=len(self.x)-1
        Ny=len(self.y)-1
        Nz=len(self.z)-1
        # creating an array of zeros, to be filled later by f(x,y,z)'s
        f=np.zeros((len(x),len(y),len(z)))
        # creating arrays of zeros (A[k] and A[k+1]), to be filled later
        A=np.zeros((2,2))
        B=np.zeros((2,2))
        ii=0
        for valx in x:
            i=np.floor(np.where(self.x<=valx)[0][-1]).astype(int)
            if (i==Nx):
                i-=1
            jj=0
            for valy in y:
                j=np.floor(np.where(self.y<=valy)[0][-1]).astype(int)
                if (j==Ny):
                    j-=1
                kk=0
                for valz in z:
                    k=np.floor(np.where(self.z<=valz)[0][-1]).astype(int)
                    if (k==Nz):
                        k-=1
                    tx = (valx-self.x[i])/self.hx[i] # calculating tx = (x-x[i])/(x[i+1]-x[i])
                    ty = (valy-self.y[j])/self.hy[j] # calculating ty = (y-y[i])/(y[i+1]-y[i])
                    tz = (valz-self.z[k])/self.hz[k] # calculating tz = (z-z[i])/(z[i+1]-z[i])
                    # calculating S(x,y,z) 
                    ptx = np.array([l1(tx),l2(tx)])
                    pty = np.array([l1(ty),l2(ty)])
                    ptz = np.array([l1(tz),l2(tz)])
                    # filling matrix A[k] and A[k+1] by f[i,j,k], f[i,j+1,k], f[i+1,j,k] and etc.
                    B[0,:]=np.array([self.f[i,j,k],self.f[i,j,k+1]])
                    B[1,:]=np.array([self.f[i+1,j,k],self.f[i+1,j,k+1]])
                    A[:,0]=np.dot(B,ptz)
                    B[0,:]=np.array([self.f[i,j+1,k],self.f[i,j+1,k+1]])
                    B[1,:]=np.array([self.f[i+1,j+1,k],self.f[i+1,j+1,k+1]])
                    A[:,1]=np.dot(B,ptz)
                    f[ii,jj,kk]=np.dot(ptx,np.dot(A,pty))
                    kk+=1
                jj+=1
            ii+=1
        return f
    #end eval3d
# end class linear interp

    
def main():

    fig1d = plt.figure()
    ax1d = fig1d.add_subplot(111)

    # 1d example
    x=np.linspace(0.,2.*np.pi,10)
    y=np.sin(x)
    lin1d=linear_interp(x=x,f=y,dims=1)
    xx=np.linspace(0.,2.*np.pi,100)
    ax1d.plot(xx,lin1d.eval1d(xx))
    ax1d.plot(x,y,'o',xx,np.sin(xx),'r--')
    ax1d.set_title('function')

    # 2d example
    fig2d = plt.figure()
    ax2d = fig2d.add_subplot(221, projection='3d')
    ax2d2 = fig2d.add_subplot(222, projection='3d')
    ax2d3 = fig2d.add_subplot(223)
    ax2d4 = fig2d.add_subplot(224)

    x=np.linspace(-2.0,2.0,11)
    y=np.linspace(-2.0,2.0,11)
    X,Y = np.meshgrid(x,y)
    Z = X*np.exp(-1.0*(X*X+Y*Y))
    ax2d.plot_wireframe(X,Y,Z)
    ax2d3.pcolor(X,Y,Z)
    #ax2d3.contourf(X,Y,Z)

    lin2d=linear_interp(x=x,y=y,f=Z,dims=2)
    x=np.linspace(-2.0,2.0,51)
    y=np.linspace(-2.0,2.0,51)
    X,Y = np.meshgrid(x,y)
    Z = lin2d.eval2d(x,y)
     
    ax2d2.plot_wireframe(X,Y,Z)
    ax2d4.pcolor(X,Y,Z)
    
    # 3d example
    x=np.linspace(0.0,3.0,10)
    y=np.linspace(0.0,3.0,10)
    z=np.linspace(0.0,3.0,10)
    X,Y,Z = np.meshgrid(x,y,z)
    F = (X+Y+Z)*np.exp(-1.0*(X*X+Y*Y+Z*Z))
    X,Y= np.meshgrid(x,y)
    fig3d=plt.figure()
    ax=fig3d.add_subplot(121)
    ax.pcolor(X,Y,F[...,int(len(z)/2)])
    lin3d=linear_interp(x=x,y=y,z=z,f=F,dims=3)
    
    x=np.linspace(0.0,3.0,50)
    y=np.linspace(0.0,3.0,50)
    z=np.linspace(0.0,3.0,50)
    X,Y= np.meshgrid(x,y)
    F=lin3d.eval3d(x,y,z)
    ax2=fig3d.add_subplot(122)
    ax2.pcolor(X,Y,F[...,int(len(z)/2)])

    plt.show()
    
    # test functions
    def test_function_1d(x):
        return np.cos(x)
    def test_function_2d(x,y):
        return (x**2)*y
    def test_function_3d(x,y,z):
        return np.sin(x*z)*y
    
    x = np.linspace(0, 10, 11)
    y = np.linspace(0, 10, 11)
    z = np.linspace(0, 10, 11)
    non_grid_x = (x[1]-x[0])/2
    y1d = test_function_1d(x) 
    y2d = test_function_2d(x,y)
    y3d = test_function_3d(x,y,z)
    s1d=linear_interp(x=x,f=y1d,dims=1) 
    
    
    
#end main
    
if __name__=="__main__":
    main()
