""" Problem 3: Programming exercise """

# import needed packages
import numpy as np

## Part a)

def first_derivative(function, x, dx):
    #  numerically estimates the first derivative of a single-argument function with O(h^2) error term
    return (function(x+dx)-function(x-dx))/(2*dx)

def test_first_derivative():
    # determines whether the first_derivative function is working or not
    x = 0.1
    dx = 1.0e-2
    tolerance = dx*10
    estimated_derivative = first_derivative(test_function, x, dx) # estimated first derivative of a known test function
    exact_derivative = first_derivative_of_test_function(x) # exact first derivative of a known test function
    
    # comparison of exact and estimated derivatives
    if np.abs(estimated_derivative - exact_derivative)<tolerance:
        print('The first derivative estimation is correct!')
    else:
        print('The first derivative estimation is NOT correct!')

def test_function(x):
    # using sine function as a known test function
    return np.sin(x)

def first_derivative_of_test_function(x):
    # using cosine function as the first derivative of the known test function (sine function)
    return np.cos(x)




## Part b)

def second_derivative(function, x, dx):
    #  numerically estimates the second derivative of a single-argument function with O(h^2) error term
    return (function(x+dx)+function(x-dx)-2*function(x))/(dx**2)

def test_second_derivative():
    # determines whether the second_derivative function is working or not
    x = 0.1
    dx = 1.0e-2
    tolerance = dx*10
    estimated_derivative = second_derivative(test_function, x, dx) # estimated second derivative of a known test function
    exact_derivative = second_derivative_of_test_function(x) # exact second derivative of a known test function
    
    # comparison of exact and estimated derivatives
    if np.abs(estimated_derivative - exact_derivative)<tolerance:
        print('The second derivative estimation is correct!')
    else:
        print('The second derivative estimation is NOT correct!')


def second_derivative_of_test_function(x):
    # using -sine function as the second derivative of the known test function (sine function)
    return -1*np.sin(x)



## Part c)

def trapezoid_integration(function, x):
    # Trapezoid rule for estimating integrals
    N = len(x)-1
    h = np.diff(x)
    s = 0
    for i in range(1, N):
        s+=((function(x[i])+function(x[i+1]))*h[i])/2
    return s

def test_trapezoid_integration():
    x = np.linspace(0,np.pi/2,100)
    xmin = x[0]
    xmax = x[len(x)-1]
    epsilon = 1.0e-2
    trapezoid_value = trapezoid_integration(test_function, x) # estimated integral of a known test function using Trapezoid rule
    exact_integral_value = integral_of_test_function(xmin, xmax) # exact integral of a known test function
    
    # comparison of values using trapezoid rule & exact integral calculation
    if np.abs(trapezoid_value - exact_integral_value)<epsilon:
        print('Trapezoid rule estimates the integral correctly!')
    else:
        print('Trapezoid rule does NOT estimate the integral correctly!')  
        
def integral_of_test_function(xmin, xmax):
    # using -cosine[N]+cosine[0] function as the exact integral of the known test function (sine function)
    return -1*np.cos(xmax)+np.cos(xmin)



## Part d)

def monte_carlo_integration(fun, xmin, xmax, blocks=10, iters=100):
    # 1D Monte Carlo integration with uniform random numbers in range [xmin,xmax]
    block_values=np.zeros((blocks,))
    L=xmax-xmin
    for block in range(blocks):
        for i in range(iters):
            x = xmin+np.random.rand()*L
            block_values[block]+=fun(x)
        block_values[block]/=iters
    I = L*np.mean(block_values) # one output of the function: the estimated value of the integral 
    dI = L*np.std(block_values)/np.sqrt(blocks) # the other output of the function: one sigma statistical error estimate (~68% reliability)
    return I,dI 

def test_monte_carlo_integration():
    xmin = 0
    xmax = np.pi/2
    blocks = 10
    iters = 100
    epsilon = 1.0e-2
    monte_carlo_value = monte_carlo_integration(test_function, xmin, xmax, blocks, iters) # estimated integral of a known test function using Monte Carlo method
    exact_integral_value = integral_of_test_function(xmin, xmax) # exact integral of a known test function
    
    # comparison of values using Monte Carlo method & exact integral calculation
    if np.abs(monte_carlo_value[0] - exact_integral_value)<epsilon:
        print('Monte Carlo method estimates the integral correctly!')
    else:
        print('Monte Carlo method does NOT estimate the integral correctly!')  

# implementation for N-dimensional numerical gradient (exercise 2, problem 4-a)
def num_gradient(function, x, dx):
    grad = np.zeros(np.size(x)) # to be filled later by gradients
    for i in range(np.size(x)):
        if np.size(x) != 1: # number of dimension > 1
            x_used_left = x[:]
            x_used_right = x[:]
            x_used_left[i] = x[i] + dx
            x_used_right[i] = x[i] - dx
        else: # number of dimension = 1
            x_used_left = x + dx
            x_used_right = x - dx
        grad[i] = (function(x_used_left)-function(x_used_right))/(2*dx)
    return grad
        
def main():
    test_first_derivative()
    test_second_derivative()
    test_trapezoid_integration()
    I,dI=monte_carlo_integration(test_function, 0., np.pi/2, 10, 100)
    print('Integrated value: {0:0.5f} +/- {1:0.5f}'.format(I, 2*dI))
    test_monte_carlo_integration()

    
if __name__=="__main__":
    main()