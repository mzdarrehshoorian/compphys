# import needed packages
import numpy as np
from scipy.integrate import simps
from matplotlib.pyplot import *
import matplotlib.pyplot as plt
from scipy.constants import epsilon_0
from spline_class import spline


""" Problem 1: 2D integral revisited """

n = 100 # grid point, by changing this accuracy is changed

def integrand(x,y): # integrand is defined
    return ((x+y)**2)*np.exp(-np.sqrt(x**2+y**2))

x = np.linspace(-2.0, 2.0, n) # x is defined
y = np.linspace(-2.0, 2.0, n) # y is defiend

# a for loop over y elements, to create double integral
integ = np.zeros(np.size(y)) # empty vector to be filled with values of integration over x later
for i in range(np.size(y)):
    integral = integrand(x,y[i])
    x_integration = simps(integral, x) # integration over x, using one value of y each time
    integ[i] = x_integration

# summing all values to obtain double integration
double_integ = np.sum(integ)
    

""" Problem 2: 2D interpolation revisited """

# Generating the experimental data
x = np.linspace(-2.0, 2.0, 30)
y = np.linspace(-2.0, 2.0, 30)
def function_xy(x,y):
    return (x+y)*np.exp(-np.sqrt(x**2+y**2))

# original plot (2D)
fig = figure()
ax = fig.add_subplot(121)
X,Y = meshgrid(x,y)
Z = function_xy(X,Y)
ax.pcolor(X,Y,Z)
ax.set_title('original')

# interpolated plot (2D)
spl2d = spline(x=x,y=y,f=Z,dims=2)
ax2 = fig.add_subplot(122)
new_x = linspace(0.0, 2.0, 100) # 0<x<2 with 100 uniformly spaced grid point
new_y = 2*(new_x**2) # along the path y=2x^2
X,Y = meshgrid(new_x,new_y)
new_Z = spl2d.eval2d(new_x,new_y)
ax2.pcolor(X,Y,new_Z)
ax2.set_title('interpolated')



""" Problem 4: Electric field """


# The 1-D differential electric field at any point in space
def differential_electric_field(r, d):
    density = 0.5 # Line charge density of a rod Q/L (assume Q=2 and L=10)
    return density/(4*np.pi*epsilon_0*((r+d)**2))

# The net field is given by integrating the differential electric field
r = np.linspace(-5, 5, 100) # lets assume L=10 m (L/2=5)
d = 2 # lets assume d=2 m
diff_elec_field = differential_electric_field(r, d)
estimated_electric_field = simps(diff_elec_field, r) # estimated value of integral

# analytical answer for electric field
def analytical_electric_field(d):
    density = 0.5 # Line charge density of a rod Q/L (assume Q=2 and L=10)
    return (density/(4*np.pi*epsilon_0))*(1/d-1/(10+d))
analytic_elec_field = analytical_electric_field(2)

